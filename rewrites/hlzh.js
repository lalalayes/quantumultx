#!name = 航旅纵横、高铁管家
#!desc = 航旅纵横悬浮标签、开屏

#航旅纵横
^http:\/\/home\.umetrip\.com\/gateway\/api\/umetrip\/native url reject
^https?:\/\/discardrp\.umetrip\.com\/gateway\/api\/umetrip\/native url reject-200
^http:\/\/umerp\.umetrip\.com\/gateway\/api\/umetrip\/native url reject

#高铁管家
^http:\/\/cdn\.133\.cn\/md\/gtgj\/.+\/.+720x1280 url reject-dict


hostname = home.umetrip.com,discardrp.umetrip.com,umerp.umetrip.com
